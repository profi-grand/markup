'use strict';



const { watch, src, dest, parallel, series } = require('gulp');
const browserSync = require("browser-sync").create();
const pug = require('gulp-pug');
const pugbem = require('gulp-pugbem');
const sass = require('gulp-sass')(require('sass'));
const plumber = require('gulp-plumber');
const autoprefixer = require('gulp-autoprefixer');
const spritesmith = require('gulp.spritesmith');

pugbem.b = true;
const appRoot = "app/";

const path = {
	pug: {
		watch:	appRoot + "pug/**/*.pug",
		src: 	appRoot + "pug/*.pug",
		dest:	appRoot,
	},
	scss: {
		watch: 	appRoot + "scss/**/*.scss",
		src: 	appRoot + "scss/main.scss",
		dest: 	appRoot + "scss/",
	},
	sprite: {
		watch: 	appRoot + "sprite/**/*.*",
		src: 	appRoot + "sprite/**/*.*",
		dest: 	appRoot + "scss/"
	},
	watch: [
		appRoot + "js/**/*.js",
		appRoot + 'css/**/*.css',
		appRoot + 'img/**/*.*',
		appRoot + 'fonts/**/*.*'
	]
};


function server(cb){
	browserSync.init({
		server: {
			baseDir: "./app"
		},
		// tunnel: true,
		host: 'localhost',
		port: 9002,
		logPrefix: "rngdv"
	});


	watch(	path.pug.watch, 		parallel(html)		);
	watch(	path.scss.watch, 		parallel(css)		);
	watch(	path.sprite.watch, 		parallel(sprite)	);

	watch(	path.watch,  function (cb) {
		browserSync.reload();
		cb();
	});
	cb();
}

function html(){
	return src(path.pug.src)
		.pipe(plumber())
		.pipe(pug({pretty: '\t', plugins: [pugbem]}))
		.pipe(dest(path.pug.dest))
		.pipe(browserSync.stream())
	;
}

function css() {
	return src(path.scss.src)
		.pipe(plumber())
		.pipe(sass({includePaths: ['./node_modules']}).on('error', sass.logError))
		// .pipe(sass({includePaths: ['./node_modules']}))
		.pipe(autoprefixer())
		.pipe(dest(path.scss.dest))
		.pipe(browserSync.stream())
	;
}

function js() {
	return src(path.scss.src)
		.pipe(plumber())
		.pipe(sass({includePaths: ['./node_modules']}))
		.pipe(autoprefixer())
		.pipe(dest(path.scss.dest))
		.pipe(browserSync.stream())
		;
}

function sprite(cb) {
	var spriteData = src(path.sprite.src).pipe(spritesmith({
		imgName: 'sprite.png',
		cssName: 'sprite.scss',
	}));

	spriteData.img.pipe(dest(path.sprite.dest));
	spriteData.css.pipe(dest(path.sprite.dest));

	browserSync.reload();
	cb();
}



exports.default = series(
	parallel(html, css, sprite),
	server
);

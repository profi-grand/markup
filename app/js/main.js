// $(".burger-menu").on('click', function () {
//   $(this).toggleClass('active');
// });
$('.slick-carousel').slick({
  infinite: false,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});


$(function () {
  //.js-contact-company-details
  // .js-contact-info


  $('.js-contact-info-item').hide().eq(0).show();
  $('.js-contact-company-item').hide().eq(0).show();
  $('.js-contacts-map').hide().eq(0).show();



  $('.js-contact-metro-item').removeClass('selected').eq(0).addClass('selected');
  $('.js-contact-metro-item :first-child').addClass('selected');
  $('.js-contact-metro-item').on('click', function (e) {
    e.stopPropagation();
    const $this = $(this);

    const id = $this.data('id');

    $('.js-contact-metro-item').removeClass('selected');
    $('.js-contact-metro-item[data-id=' + id + ']').addClass('selected');

    $('.js-contact-info-item').hide();
    $('.js-contact-info-item[data-id=' + id + ']').show();

    $('.js-contact-company-item').hide();
    $('.js-contact-company-item[data-id=' + id + ']').show();

    $('.js-contact-map-item').hide();
    $('.js-contact-map-item[data-id=' + id + ']').show();
  });



  $('.js-contact-tabs').children().removeClass('selected');
  $('.js-contact-tabs :first-child').addClass('selected');
  $('.js-contact-tabs').children().on('click', function () {
    const block = $(this).data('block');

    $('.js-contact-tabs').children().removeClass('selected');
    $('.js-contact-tabs [data-block=' + block + ']').addClass('selected');

    $('.js-contact-block').hide();
    $('.js-contact-' + block).show();
  });


  $('.js-toggle-filter').on('click', function () {
    const isActive = $('.js-price-filter').toggleClass('active').hasClass('active');

    this.innerHTML = isActive ? 'Скрыть фильтры' : 'Показать фильтры';
  });


  $('.js-menu-category').on('click', function () {
    $(this).parent().toggleClass('active');
  });

  const qtySlider = document.querySelector('.js-qty-slider');
  if (qtySlider) {
    noUiSlider.create(qtySlider, {
      start: [1],
      step: 1,
      connect: [true, false],
      range: {
        min: 1,
        max: 10
      }
    });
  }




  const priceRange = document.querySelector('.js-price-range');

  if (priceRange) {
    const min = parseInt($('.js-price-filter [name="from"]').val());
    const max = parseInt($('.js-price-filter [name="to"]').val());
    noUiSlider.create(priceRange, {
      start: [min, max],
      // snap: true,
      connect: true,
      range: {
        'min': min,
        'max': max,
      }
    });

    priceRange.noUiSlider.on('update', function (values, handle) {
      $('.js-price-filter [name="from"]').val(parseInt(values[0]));
      $('.js-price-filter [name="to"]').val(parseInt(values[1]));
    });

    $('.js-price-filter input').on('change', function () {
      const $minInput = $('.js-price-filter [name="from"]');
      const $maxInput = $('.js-price-filter [name="to"]');

      const min = parseInt($minInput.val()) || -1;
      const max = parseInt($maxInput.val()) || -1;

      priceRange.noUiSlider.set([min, max]);
    });
  }


  $('.js-slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.js-slider-nav'
  });
  $('.js-slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.js-slider-for',
    arrows: false,
    // dots: true,
    // centerMode: true,
    focusOnSelect: true
  });


  $('.js-tabs').children().on('click', function () {
    $(this).parent().children().removeClass('selected');
    $(this).addClass('selected');
  });


  $('[data-product-tab]').on('click', function () {
    $('[data-for-product-tab]').hide();
    $('[data-for-product-tab="' + $(this).data('product-tab') + '"]').show();
  });


  $('.js-form-send').on('click', function () {
    $('.js-form-content').hide();
    $('.js-form-success').show();
  })



  ymaps.ready(function(){
    $('[data-lat][data-lon]').each(function () {
      const $this = $(this);
      const address = $this.data('adr');
      const lat = parseFloat($this.data('lat'));
      const lon = parseFloat($this.data('lon'));

      const myMap = new ymaps.Map(this, {
        center:[lat, lon],
        zoom:17,
        controls: []
      });

      myMap.geoObjects.add(
        new ymaps.Placemark(
          [ lat, lon ],
          { balloonContent: address },
        )
      );
    });
  });

});
